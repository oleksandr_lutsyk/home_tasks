def str_to_tuple(string):
    return tuple(ord(char) for char in string)

if __name__ == '__main__':
    print(str_to_tuple('Hello world'))