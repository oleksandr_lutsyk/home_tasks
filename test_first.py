# import unittest
from first import str_to_tuple

test_values = {'a' : (97,),
             'aaa' : (97, 97, 97),
             '123' : (49, 50, 51),
             '[Xx1 y@' : (91, 88, 120, 49, 32, 121, 64),}


def test_str_to_tuple(test_data):
    for k in test_data.keys():
        if str_to_tuple(k) != test_data[k]:
            print('Test not passed')
            return False
    print('Test passed')
    return True

if __name__ == '__main__':
    test_str_to_tuple(test_values)